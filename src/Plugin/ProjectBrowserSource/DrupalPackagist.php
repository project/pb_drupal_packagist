<?php

namespace Drupal\pb_drupal_packagist\Plugin\ProjectBrowserSource;

use Drupal\Component\Serialization\Json;
use Drupal\project_browser\Plugin\ProjectBrowserSourceBase;
use Drupal\project_browser\ProjectBrowser\Project;
use Drupal\project_browser\ProjectBrowser\ProjectsResultsPage;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Project Browser Source Plugin Drupal Packagist.
 *
 * @ProjectBrowserSource(
 *   id = "project_browser_drupal_packagist",
 *   label = @Translation("Drupal Packagist"),
 *   description = @Translation("Source plugin for Project Browser to search Drupal packagist."),
 * )
 */
class DrupalPackagist extends ProjectBrowserSourceBase {

  /**
   * Search endpoint.
   *
   * @const string
   */
  const ENDPOINT = 'https://packages.drupal.org/8/search.json';

  /**
   * Constructor for the plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected ClientInterface $httpClient,
    protected LoggerInterface $logger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.factory')->get('project_browser'),
    );
  }

  /**
   * Performs a request to the jsonapi and returns the results.
   *
   * @param string $search
   *   Search parameter.
   * @param int $page
   *   Results page number to get.
   * @param string $prefix
   *   Prefix to add to non-namespaced search strings.
   *
   * @return array
   *   Results from the query.
   */
  protected function packagistSearch($search, $page = 1, $prefix = 'drupal/') {
    if (strpos($search, '/') === FALSE) {
      $search = $prefix . $search;
    }
    if (empty($page) || !is_numeric($page)) {
      $page = 1;
    }

    $result = [
      'code' => NULL,
      'data' => NULL,
      'message' => '',
    ];
    $params = [];
    try {
      if (!empty($search)) {
        $params = [
          'query' => [
            's' => $search,
            'page' => $page,
          ],
        ];
      }

      $response = $this->httpClient->request('GET', self::ENDPOINT, $params);
      $response_data = Json::decode($response->getBody()->getContents());

      $result['code'] = $response->getStatusCode();
      $result['data'] = $response_data['results'];
      $result['total'] = $response_data['total'] ?? NULL;
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      $result['message'] = $exception->getMessage();
      $result['code'] = $exception->getCode();
    }
    catch (\Throwable $exception) {
      $this->logger->error($exception->getMessage());
      $result['message'] = $exception->getMessage();
      $result['code'] = Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjects(array $query = []): ProjectsResultsPage {
    $machine_name_filter = $query['machine_name'] ?? '';
    $projects_from_source = $this->packagistSearch($query['search'] ?? '', ($query['page']+1));

    $projects = [];
    foreach ($projects_from_source['data'] as $project_from_source) {
      $module_machine_name = str_replace('drupal/', '', $project_from_source['name']);
      $logo = [];
      if (FALSE && strpos($project_from_source['name'], 'drupal/') !== FALSE) {
        // Best guess, otherwise it will fallback into default image.
        $logo = [
          'file' => [
            'uri' => 'https://git.drupalcode.org/project/' . $module_machine_name . '/-/avatar',
            'resource' => 'image',
          ],
          'alt' => $module_machine_name . ' logo',
        ];
      }

      $project = new Project(
        logo: $logo,
        isCompatible: TRUE,
        isMaintained: TRUE,
        isCovered: TRUE,
        isActive: TRUE,
        starUserCount: 0,
        projectUsageTotal: 0,
        machineName: $project_from_source['name'],
        body: [
          'summary' => strip_tags($project_from_source['description']),
          'value' => $project_from_source['description'],
        ],
        title: ucwords($module_machine_name),
        changed: 0,
        created: 0,
        author: [],
        packageName: $project_from_source['name'],
        categories: [],
        images: [],
      );

      if ($project_from_source['name'] === $machine_name_filter) {
        $projects = [$project];
        $project_from_source['total'] = 1;
        // Do not continue checking the loop.
        break;
      }
      else {
        $projects[] = $project;
      }
    }

    return $this->createResultsPage($projects, $projects_from_source['total'] ?? 0);
  }

  /**
   * {@inheritdoc}
   */
  public function getCategories(): array {
    return [];
  }

  /**
   * Returns the available sort options that plugins will parse.
   *
   * @return array
   *   Options offered.
   */
  public function getSortOptions(): array {
    return [
      'default' => [
        'id' => 'default',
        'text' => $this->t('Default'),
      ],
    ];
  }

}
